package com.huawei.w00466247.client.controller;
import com.huawei.w00466247.client.util.HttpUtil;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
* @ClassName ConfigRefreshController
* @Description
* @Author w00466247
* @Date 2020/7/2 19:22
* @Version 1.0
**/
@RestController
@Api(tags = "cloud-config-refresh", description = "配置管理")
@RequestMapping(value = "/config")
public class ConfigRefreshController {
@GetMapping(value = "/configRefresh")
public Object configValues(){
try{
HttpUtil.sendPost("http://10.136.86.187:8888/bus/refresh",null,"");
return "success";
}catch (Exception e){
return "error";
}
}
}
