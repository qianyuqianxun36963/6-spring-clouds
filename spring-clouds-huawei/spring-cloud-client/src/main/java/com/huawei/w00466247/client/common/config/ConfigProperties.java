package com.huawei.w00466247.client.common.config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
* @ClassName ConfigProperties
* @Description
* @Author w00466247
* @Date 2020/7/1 19:15
* @Version 1.0
**/
@Component
@ConfigurationProperties(prefix = "data")
public class ConfigProperties {
public static class UserInfo {
private String username;
private String password;
public String getUsername() {
return username;
}
public void setUsername(String username) {
this.username = username;
}
public String getPassword() {
return password;
}
public void setPassword(String password) {
this.password = password;
}
@Override
public String toString() {
return "UserInfo{" +
"username='" + username + '\'' +
", password='" + password + '\'' +
'}';
}
}
private String env;
private UserInfo user;
public String getEnv() {
return env;
}
public ConfigProperties setEnv(String env) {
this.env = env;
return this;
}
public UserInfo getUser() {
return user;
}
public ConfigProperties setUser(UserInfo user) {
this.user = user;
return this;
}
}
