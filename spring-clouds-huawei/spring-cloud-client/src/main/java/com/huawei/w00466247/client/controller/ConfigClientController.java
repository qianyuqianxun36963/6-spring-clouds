package com.huawei.w00466247.client.controller;
import com.huawei.w00466247.client.common.config.ConfigProperties;
import com.huawei.w00466247.client.common.config.ConfigValues;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
* @ClassName ConfigClientController
* @Description
* @Author w00466247
* @Date 2020/7/1 15:13
* @Version 1.0
**/
@RestController
@Api(tags = "cloud-config-client", description = "配置管理")
@RequestMapping(value = "/config")
public class ConfigClientController {
@Autowired
ConfigValues configValues;
@Autowired
ConfigProperties configProperties;
@GetMapping(value = "/configValues")
public Object configValues(){
return configValues;
}
@GetMapping(value = "/configProperties")
public Object configProperties(){
return configProperties;
}
}
