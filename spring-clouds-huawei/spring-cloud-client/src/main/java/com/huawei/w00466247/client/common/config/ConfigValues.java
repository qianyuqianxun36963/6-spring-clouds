package com.huawei.w00466247.client.common.config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
* @ClassName Property
* @Description
* @Author w00466247
* @Date 2020/7/1 17:28
* @Version 1.0
**/
@Component
public class ConfigValues {
@Value("${data.env}")
private String env;
@Value("${data.user.username}")
private String username;
@Value("${data.user.password}")
private String password;
public String getEnv() {
return env;
}
public ConfigValues setEnv(String env) {
this.env = env;
return this;
}
public String getUsername() {
return username;
}
public ConfigValues setUsername(String username) {
this.username = username;
return this;
}
public String getPassword() {
return password;
}
public ConfigValues setPassword(String password) {
this.password = password;
return this;
}
}
