package com.huawei.w00466247.client.util;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.CharArrayBuffer;
import org.apache.http.util.EntityUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
/**
* @ClassName HttpUtil
* @Description
* @Author w00466247
* @Date 2019/11/15 18:39
* @Version 1.0
**/
public class HttpUtil {
private static final int SO_TIMEOUT = 86400000;
public static HttpResponse sendRequestByGet(String url, String requestBodys) throws Exception
{
return sendRequestByGet(url, requestBodys, null);
}
/**
* @Description Get请求获取远程数据
* @Param [url, requestBodys, headers]
* @return HttpResponse
**/
public static HttpResponse sendRequestByGet(String url, String requestBodys, Map<String, String> headers)
throws Exception
{
HttpResponse httpResponse = new HttpResponse();
// 构造HttpClient的实例
HttpClient httpClient = new HttpClient();
GetMethod getMethod = new GetMethod(url);
httpClient.getHttpConnectionManager().getParams().setSoTimeout(SO_TIMEOUT);
if (headers != null && headers.size() > 0)
{
for (Entry<String, String> entry : headers.entrySet())
{
getMethod.setRequestHeader(entry.getKey(), entry.getValue());
}
}
getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
try
{
getMethod.setQueryString(requestBodys);
// 执行getMethod
int statusCode = httpClient.executeMethod(getMethod);
httpResponse.setResponseStatus(statusCode);
fillResponseContent(getMethod,httpResponse);
}
catch (IOException e)
{
throw e;
}
finally
{
// 释放连接
getMethod.releaseConnection();
}
return httpResponse;
}
/**
* @Description Post请求获取远程数据
* @Param [url, requestBody, contentType, charset]
* @return HttpResponse
**/
public static HttpResponse sendRequestByPost(String url, Map<String,String> requestParams, Map<String,String> requestBody, String contentType, String charset)
throws Exception
{
HttpResponse httpResponse = new HttpResponse();
// 构造HttpClient的实例
HttpClient httpClient = new HttpClient();
PostMethod postMethod = new PostMethod(url);
httpClient.getHttpConnectionManager().getParams().setSoTimeout(SO_TIMEOUT);
postMethod.getParams().setParameter(contentType,charset);
BufferedReader reader = null;
try
{
if(null!=requestParams)
{
NameValuePair[] list = new NameValuePair[requestParams.size()];
int index =0;
for(String key:requestParams.keySet())
{
list[index] = new NameValuePair(key,requestParams.get(key));
index++;
}
postMethod.setQueryString(list);
}
if(null!=requestBody)
{
NameValuePair[] bodyList = new NameValuePair[requestBody.size()];
int bodyIndex =0;
for(String key:requestBody.keySet())
{
bodyList[bodyIndex] = new NameValuePair(key,requestBody.get(key));
bodyIndex++;
}
postMethod.setRequestBody(bodyList);
}
// 执行postMethod
int statusCode = httpClient.executeMethod(postMethod);
httpResponse.setResponseStatus(statusCode);
fillResponseContent(postMethod,httpResponse);
}
catch (IOException e)
{
throw e;
}
finally
{
// 释放连接
postMethod.releaseConnection();
}
return httpResponse;
}
/**
* Put请求
*
* @param url url
* @param requestBody requestBody
* @param contentType contentType
* @param charset charset
* @return HttpResponse
* @throws Exception Exception
*/
public static HttpResponse sendRequestByPut(String url, String requestBody, String contentType, String charset)
throws Exception
{
HttpResponse httpResponse = new HttpResponse();
// 构造HttpClient的实例
HttpClient httpClient = new HttpClient();
PutMethod putMethod = new PutMethod(url);
putMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
putMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, SO_TIMEOUT);
BufferedReader reader = null;
try
{
RequestEntity requestEntity = new StringRequestEntity(requestBody, contentType, charset);
putMethod.setRequestEntity(requestEntity);
// 执行postMethod
int statusCode = httpClient.executeMethod(putMethod);
httpResponse.setResponseStatus(statusCode);
// 读取返回的内容
if (putMethod.getResponseBodyAsStream() != null)
{
InputStreamReader inputStreamReader = new InputStreamReader(putMethod.getResponseBodyAsStream(), "UTF-8");
reader = new BufferedReader(inputStreamReader);
StringBuffer stringBuffer = new StringBuffer();
String str = "";
while ((str = reader.readLine()) != null)
{
stringBuffer.append(str);
stringBuffer.append("\n");
}
httpResponse.setResponseContent(stringBuffer.length() == 0 ? "" : stringBuffer.deleteCharAt(stringBuffer.length() - 1).toString());
}
}
catch (IOException e)
{
throw e;
}
finally
{
if (null != reader)
{
reader.close();
}
// 释放连接
putMethod.releaseConnection();
}
return httpResponse;
}
private static void fillResponseContent(HttpMethodBase httpmethod,HttpResponse httpResponse) throws Exception{
BufferedReader reader = null;
try{
// 读取返回的内容
if (httpmethod.getResponseBodyAsStream() != null)
{
InputStreamReader inputStreamReader = new InputStreamReader(httpmethod.getResponseBodyAsStream(), "UTF-8");
reader = new BufferedReader(inputStreamReader);
StringBuffer stringBuffer = new StringBuffer();
String str = "";
while ((str = reader.readLine()) != null)
{
stringBuffer.append(str);
stringBuffer.append("\n");
}
httpResponse.setResponseContent(stringBuffer.length() == 0 ? "" : stringBuffer.deleteCharAt(stringBuffer.length() - 1).toString());
}
}catch (Exception e){
e.printStackTrace();
}finally {
if (null != reader)
{
reader.close();
}
}
}
// 下面几个是后来新加的方法，需要添加 httpcomponents 的三个maven依赖
/**
* get请求，参数拼接在地址上
*
* @param
* @return 响应
*/
public static String get(String url) {
return get(url,null,null);
}
/**
* get请求，参数放在map里
*
* @param url 请求地址
* @param map 参数map
* @return 响应
*/
public static String get(String url, Map<String, String> map, Map<String, String> headers) {
int timeOut = 1200 * 1000;
String result = null;
CloseableHttpClient httpClient = HttpClients.createDefault();
CloseableHttpResponse response = null;
try {
HttpGet get;
if (map == null || map.size() == 0) {
get = new HttpGet(url);
} else {
List<org.apache.http.NameValuePair> pairs = new ArrayList<org.apache.http.NameValuePair>();
for (Entry<String, String> entry : map.entrySet()) {
pairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
}
URIBuilder builder = new URIBuilder(url);
builder.setParameters(pairs);
get = new HttpGet(builder.build());
}
// 构建自定义header
if (headers != null) {
for (Entry<String, String> header : headers.entrySet()) {
get.setHeader(header.getKey(), header.getValue());
}
}
RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeOut).setSocketTimeout(timeOut).build();
get.setConfig(requestConfig);
response = httpClient.execute(get);
//&& response.getStatusLine().getStatusCode() == 200
if (response != null ) {
HttpEntity entity = response.getEntity();
result = entityToString(entity);
}
return result;
} catch (URISyntaxException e) {
e.printStackTrace();
} catch (ClientProtocolException e) {
e.printStackTrace();
} catch (IOException e) {
e.printStackTrace();
} finally {
try {
httpClient.close();
if (response != null) {
response.close();
}
} catch (IOException e) {
e.printStackTrace();
}
}
return null;
}
/**
* post请求，参数为json字符串
*
* @param url 请求地址
* @param jsonString json字符串
* @return 响应
*/
public static String post(String url, Map<String, String> header,String jsonString) {
return sendPost(url,header,jsonString);
}
// 有时post请求也会有 ?pid=001&pageSize=100 这种拼凑在url后面的参数。
public static String post(String url, Map<String, String> header,Map<String,String> queryParam,String jsonString) {
StringBuffer sb = new StringBuffer();
if(!queryParam.isEmpty()){
for(String key:queryParam.keySet()){
sb.append("&").append(key).append("=").append(queryParam.get(key));
}
}
String postfix = sb.toString().replaceFirst("&","?");
return sendPost(url+postfix,header,jsonString);
}
public static String sendPost (String url,Map<String, String> header, String jsonString) {
int timeOut = 3600 * 1000;
HttpPost httppost = new HttpPost(url);
if (StringUtils.isNotBlank(jsonString)){
StringEntity entity = new StringEntity(jsonString, Consts.UTF_8);
httppost.setEntity(entity);
}
CloseableHttpClient httpClient = HttpClients.createDefault();
RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeOut).setSocketTimeout(timeOut).build();
httppost.setConfig(requestConfig);
if(header!=null){
for(String key : header.keySet()) {
httppost.setHeader(key, header.get(key));
}
}
CloseableHttpResponse response = null;
try {
response =httpClient.execute(httppost);
} catch (IOException e) {
e.printStackTrace();
}
HttpEntity entity1 = response.getEntity();
String result = null;
try {
result = EntityUtils.toString(entity1);
} catch (ParseException | IOException e) {
e.printStackTrace();
}
return result;
}
private static String entityToString(HttpEntity entity) throws IOException {
String result = null;
if (entity != null) {
long lenth = entity.getContentLength();
if (lenth != -1 && lenth < 2048) {
result = EntityUtils.toString(entity, "UTF-8");
} else {
InputStreamReader reader1 = new InputStreamReader(entity.getContent(), "UTF-8");
CharArrayBuffer buffer = new CharArrayBuffer(2048);
char[] tmp = new char[1024];
int l;
while ((l = reader1.read(tmp)) != -1) {
buffer.append(tmp, 0, l);
}
result = buffer.toString();
}
}
return result;
}
}
