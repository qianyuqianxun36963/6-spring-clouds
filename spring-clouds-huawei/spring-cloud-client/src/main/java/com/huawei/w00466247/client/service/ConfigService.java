package com.huawei.w00466247.client.service;
import com.huawei.w00466247.client.common.config.ConfigValues;
import com.huawei.w00466247.client.common.config.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
/**
* @ClassName ConfigService
* @Description
* @Author w00466247
* @Date 2020/7/2 11:36
* @Version 1.0
**/
@Service
@RefreshScope
public class ConfigService {
@Autowired
ConfigValues configValues;
@Autowired
ConfigProperties configProperties;
public ConfigValues getConfigValues() {
return configValues;
}
public ConfigService setConfigValues(ConfigValues configValues) {
this.configValues = configValues;
return this;
}
public ConfigProperties getConfigProperties() {
return configProperties;
}
public ConfigService setConfigProperties(ConfigProperties configProperties) {
this.configProperties = configProperties;
return this;
}
}
