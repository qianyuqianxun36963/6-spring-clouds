package com.huawei.w00466247.zuul.filter;
import com.netflix.zuul.ZuulFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
@Component
public class HttpRequestFilter extends ZuulFilter {
private final static Logger logger = LoggerFactory.getLogger(HttpRequestFilter.class);
@Override
public String filterType() {
return "pre";
}
@Override
public int filterOrder() {
return 0;
}
@Override
public boolean shouldFilter() {
return true;
}
@Override
public Object run() {
logger.info("This is a pre-filter for intercepting the HttpRequest's basic information. ");
getRequestInfo();
return null;
}
public void getRequestInfo() {
ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
HttpServletRequest request = attributes.getRequest();
logger.info("ip = {}", request.getRemoteAddr());
logger.info("uri = {}", request.getRequestURI());
logger.info("method = {}", request.getMethod());
Long creationTime = request.getSession().getCreationTime();
SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
Date dt = new Date(creationTime);
String sDateTime = sdf.format(dt);
logger.info("creation_time = {}", sDateTime);
}
}
