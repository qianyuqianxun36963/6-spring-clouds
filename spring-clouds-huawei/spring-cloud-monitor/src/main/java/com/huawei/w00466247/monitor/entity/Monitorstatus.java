package com.huawei.w00466247.monitor.entity;
import javax.persistence.*;
/**
* @author w00466247
* @time 2017/11/29 17:05
*/
@Entity
@IdClass(MonitorstatusPK.class)
@Table(name = "t_monitorstatus")
public class Monitorstatus {
private String ip;
private int port;
private String status;
private String operationStatus;
private String servicename;
@Id
@Column(name = "ip", nullable = false, length = 20)
public String getIp() {
return ip;
}
public void setIp(String ip) {
this.ip = ip;
}
@Id
@Column(name = "port", nullable = false)
public int getPort() {
return port;
}
public void setPort(int port) {
this.port = port;
}
@Basic
@Column(name = "status", nullable = false, length = 20)
public String getStatus() {
return status;
}
public void setStatus(String status) {
this.status = status;
}
@Basic
@Column(name = "operation_status", nullable = true, length = 20)
public String getOperationStatus() {
return operationStatus;
}
public void setOperationStatus(String operationStatus) {
this.operationStatus = operationStatus;
}
@Basic
@Column(name = "servicename", nullable = false, length = 100)
public String getServicename() {
return servicename;
}
public void setServicename(String servicename) {
this.servicename = servicename;
}
@Override
public boolean equals(Object o) {
if (this == o) return true;
if (o == null || getClass() != o.getClass()) return false;
Monitorstatus that = (Monitorstatus) o;
if (port != that.port) return false;
if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
if (status != null ? !status.equals(that.status) : that.status != null) return false;
if (operationStatus != null ? !operationStatus.equals(that.operationStatus) : that.operationStatus != null)
return false;
if (servicename != null ? !servicename.equals(that.servicename) : that.servicename != null) return false;
return true;
}
@Override
public int hashCode() {
int result = ip != null ? ip.hashCode() : 0;
result = 31 * result + port;
result = 31 * result + (status != null ? status.hashCode() : 0);
result = 31 * result + (operationStatus != null ? operationStatus.hashCode() : 0);
result = 31 * result + (servicename != null ? servicename.hashCode() : 0);
return result;
}
}
