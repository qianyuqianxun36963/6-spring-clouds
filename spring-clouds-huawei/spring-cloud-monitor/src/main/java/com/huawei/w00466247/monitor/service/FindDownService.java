package com.huawei.w00466247.monitor.service;
import com.huawei.w00466247.monitor.entity.Monitorstatus;
import com.huawei.w00466247.monitor.repository.MonitorstatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
* @author w00466247
* @time 2018/3/14 15:32
*/
@Service
public class FindDownService {
@Autowired
private DiscoveryClient discoveryClient;
@Autowired
private MonitorstatusRepository monitorstatus;
@Autowired
private HttpGetService httpGetService;
@Autowired
private SendMailService sendMailService;
public List<Monitorstatus> findDown() {
//记录可用的服务列表
List<Monitorstatus> upList = new ArrayList<>();
//记录不可用的服务列表
List<Monitorstatus> downList;
//获取到所有可用的服务名
List<String> serviceList = discoveryClient.getServices();
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
String time = df.format(new Date());
String exceptionStr = "";
System.out.println(serviceList);
for (String s : serviceList) {
List<ServiceInstance> list = discoveryClient.getInstances(s);
if (list != null && list.size() > 0) {
for (ServiceInstance l : list) {
Monitorstatus m = new Monitorstatus();
m.setServicename(s);
try {
String ip = InetAddress.getByName(l.getHost()).getHostAddress();
m.setIp(ip);
m.setPort(l.getPort());
m.setStatus("UP-SERVING");
upList.add(m);
} catch (UnknownHostException e) {
// e.printStackTrace();
String ip = l.getHost();
exceptionStr += "时间：" + time + ", 有不知名主机混进来啦：\n主机名:" + ip + "，服务名:" + s + "\n";
} catch (Exception e) {
e.printStackTrace();
exceptionStr += "时间：" + time + ", 程序出现了一些错误：" + e.getMessage() + "\n";
}
}
}
}
System.out.println("upList: " + upList);
//将新增的微服务实例添加到数据库中
for (Monitorstatus monitor : upList) {
//判断实例是否确实可用
String url = "http://" + monitor.getIp() + ":" + monitor.getPort() + "/health";
String s = httpGetService.sendGet(url, null);
if ("wrong".equals(s) || s.contains("\"status\":\"DOWN\"")) {
monitor.setStatus("DOWN-UNSERVING");
}
System.out.println(s);
monitorstatus.save(monitor);
}
List<Monitorstatus> allList = monitorstatus.findAll();
System.out.println("modify-allList: " + allList);
//判断数据库哪些服务不在可用列表中,标记为down
for (Monitorstatus monitor : allList) {
boolean isExit = false;
for (Monitorstatus u : upList) {
if (u.getIp().equals(monitor.getIp()) && u.getPort() == monitor.getPort()) {
isExit = true;
}
}
if (!isExit) {
monitor.setStatus("DOWN-UNSERVING");
monitorstatus.save(monitor);
}
System.out.println("服务名：" + monitor.getServicename());
if ("upload-xmltestbed-to-lcm".equals(monitor.getServicename())) {
monitor.setStatus("UP-SERVING");
monitorstatus.save(monitor);
}
}
downList = monitorstatus.findByStatus("DOWN-UNSERVING");
System.out.println("downList： " + downList);
String str = "时间：" + time + ", 以下服务不可用，请及时查看：\n";
for (Monitorstatus m : downList) {
str += "服务名：" + m.getServicename() + ", 执行机IP: " + m.getIp() + ", 端口Port: " + m.getPort() + "\n";
}
str += "其他异常：\n" + exceptionStr;
if (downList.size() > 0 || !exceptionStr.equals("")) {
sendMailService.sendMail(str);
}
return downList;
}
}
