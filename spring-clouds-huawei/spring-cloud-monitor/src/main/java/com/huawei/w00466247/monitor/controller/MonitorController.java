package com.huawei.w00466247.monitor.controller;
import com.huawei.w00466247.monitor.repository.MonitorstatusRepository;
import com.huawei.w00466247.monitor.entity.Monitorstatus;
import com.huawei.w00466247.monitor.service.FindDownService;
import com.huawei.w00466247.monitor.service.SendMailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
/**
* @author w00466247
* @time 2017/11/29 17:25
*/
@Api(description = "监控微服务状态")
@RestController
public class MonitorController {
private final Logger logger = Logger.getLogger(getClass());
@Autowired
private MonitorstatusRepository monitorstatus;
@Autowired
private FindDownService findDownService;
@Autowired
SendMailService sendMailService;
@ApiOperation("返回数据库中所有的微服务实例列表")
@GetMapping("/findAll")
public List<Monitorstatus> findAll(){
return monitorstatus.findAll();
}
@ApiOperation("返回不可用的微服务实例列表")
@GetMapping("/findDown")
public List<Monitorstatus> findDown() throws Exception{
return findDownService.findDown();
}
@ApiOperation("发送邮件")
@PostMapping("/sendMail")
public String sendMail(@RequestBody String s) throws Exception{
sendMailService.sendMail(s);
return "end.";
}
}
