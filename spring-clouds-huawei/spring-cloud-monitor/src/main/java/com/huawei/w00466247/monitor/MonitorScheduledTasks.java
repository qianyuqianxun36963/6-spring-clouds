package com.huawei.w00466247.monitor;
import com.huawei.w00466247.monitor.service.FindDownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
/**
* @author w00466247
* @time 2018/3/15 14:43
*/
@Component
@EnableScheduling
public class MonitorScheduledTasks {
@Autowired
FindDownService findDownService;
@Scheduled(fixedDelay = 600000)
public void monitorMicroService() {
findDownService.findDown();
}
}
