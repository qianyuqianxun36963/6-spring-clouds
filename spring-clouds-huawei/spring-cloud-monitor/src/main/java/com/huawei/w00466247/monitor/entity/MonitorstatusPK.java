package com.huawei.w00466247.monitor.entity;
import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
/**
* @author w00466247
* @time 2017/11/30 9:21
*/
public class MonitorstatusPK implements Serializable {
private String ip;
private int port;
@Column(name = "ip", nullable = false, length = 20)
@Id
public String getIp() {
return ip;
}
public void setIp(String ip) {
this.ip = ip;
}
@Column(name = "port", nullable = false)
@Id
public int getPort() {
return port;
}
public void setPort(int port) {
this.port = port;
}
@Override
public boolean equals(Object o) {
if (this == o) return true;
if (o == null || getClass() != o.getClass()) return false;
MonitorstatusPK that = (MonitorstatusPK) o;
if (port != that.port) return false;
if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
return true;
}
@Override
public int hashCode() {
int result = ip != null ? ip.hashCode() : 0;
result = 31 * result + port;
return result;
}
}
