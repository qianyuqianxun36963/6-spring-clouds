package com.huawei.w00466247.monitor.repository;
import com.huawei.w00466247.monitor.entity.MonitorstatusPK;
import com.huawei.w00466247.monitor.entity.Monitorstatus;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
/**
* @author w00466247
* @time 2017/11/29 17:06
*/
public interface MonitorstatusRepository extends JpaRepository<Monitorstatus, MonitorstatusPK> {
Monitorstatus findByIpAndPort(String ip,Integer port);
List<Monitorstatus> findByStatus(String status);
}
