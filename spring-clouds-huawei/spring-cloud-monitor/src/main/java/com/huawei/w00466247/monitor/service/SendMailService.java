package com.huawei.w00466247.monitor.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import java.util.Date;
/**
* @author w00466247
* @time 2017/11/30 19:28
*/
@Service
public class SendMailService {
@Autowired
private JavaMailSender mailSender;
public String sendMail(String msg) {
SimpleMailMessage message = new SimpleMailMessage();
message.setFrom("chenjian118@huawei.com");
// message.setTo("chenhuiqiong2@huawei.com");
message.setTo("chenjian118@huawei.com");
message.setSubject("【微服务告警】");
message.setSentDate(new Date());
message.setText(msg);
if (!msg.contains("10.134.192.138")) {
mailSender.send(message);
}
return "已发送";
}
}
